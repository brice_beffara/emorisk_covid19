library(tidyverse)
devtools::install_github("ndphillips/yarrr", 
                         build_vignettes = TRUE)

install.packages('BayesFactor', dependencies = TRUE)
install.packages("lme4")
library(yarrr)
library(lme4)

# install.packages("rapport")

covid_df <- read_csv("EMORISKCOVID_19_26 mars 2020_15.13.csv")

# Selecting dates (5 days after lauchin the questionnaire) according to the preregistration
dates_prereg <- c("2020-03-20",
                  "2020-03-21",
                  "2020-03-22",
                  "2020-03-23",
                  "2020-03-24",
                  "2020-03-25")

covid_df <- covid_df %>%
  # Filter by preregistered dates and attention check
  filter(Q57 == "Oui") %>%
  filter(Q58 == "J'ai participé sérieusement") %>%
  filter(str_detect(EndDate, glue_collapse(dates_prereg, "|"))) %>%
  # Keep only real responses, not previews
  filter(Status == "IP Address") %>%
  # Keep only people >= 18 years (Otherwise empty responses)
  filter(Q64 == "Oui") %>%
  # Keep only people living in France (Otherwise empty responses)
  filter(Q65 == "Oui") %>%
  # Remove people who gave a response with an empty space for the first question (n = 11)
  unite(amount,
        endo:exo,
        sep = "",
        remove = FALSE,
        na.rm = TRUE) %>%
  
  filter(amount != "") %>%
  mutate(amount = as.numeric(amount)) %>%
  # Applying the preregistered IQR filter
  filter(
    amount > quantile(amount, 0.25) - 3 * (quantile(amount, 0.75) - quantile(amount, 0.25)),
    amount < quantile(amount, 0.75) + 3 * (quantile(amount, 0.75) - quantile(amount, 0.25))
  ) 

library(ggplot2)
install.packages("ggplot2")

ggplot(covid_df, aes(y=amount, x=condgrp)) + 
  geom_boxplot() 

plot(amount ~ condgrp, data = covid_df)
lm(amount ~ condgrp, data = covid_df)

rega6 <- covid_df %>%
  count(vars = condgrp)
#
#boxplot(covid_df$amount)
#mean(covid_df$amount)
#median(covid_df$amount)
